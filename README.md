# Elastic Stack (ELK+B) utilizando Docker-compose

[![Elastic Stack version](https://img.shields.io/badge/Elastic%20Stack-7.10.1-00bfb3?style=flat&logo=elastic-stack)](https://www.elastic.co/blog/category/releases)

Vamos falar de Elastic Stack, ou ELK Stack (**E**lasticsearch + **L**ogstash + **K**ibana + **B**eats), esse conjunto de ferramentas da [Elastic](https://www.elastic.co/pt/what-is/elk-stack) que tem o poder de coletar, centralizar e apresentar seus dados de forma profissional.

O ELK Stack é composto pelas ferramentas abaixo:

* [Elasticsearch](https://github.com/elastic/elasticsearch/tree/master): Mecanismo de pesquisa RESTful distribuído que armazena todos os dados coletados. 
* [Logstash](https://github.com/elastic/logstash/tree/master/docker): Componente de processamento de dados do Elastic Stack que recebe, transforma e envia dados para o Elasticsearch. 
* [Kibana](https://github.com/elastic/kibana/tree/master): Interface web para pesquisa, criação e visualização de gráficos e dashboards. 
* [Beats](https://github.com/elastic/beats/tree/master): Coleção de agentes que são instalados em locais especificos que tem a funçao de levar dados para o elasticsearch

![](img/elk-stack-elkb-diagram.svg)

## Requisitos

* [Docker Engine](https://docs.docker.com/install/) versão **17.05** ou superior
* [Docker Compose](https://docs.docker.com/compose/install/) versão **1.20.0** ou superior
* [OpenJDK](https://openjdk.java.net/install/) versão **8** ou superior
* 1.5 GB of RAM

## Portas expostas

* 9200: Elasticsearch HTTP
* 9300: Elasticsearch TCP transport
* 9600: Logstash monitoring API
* 8089: Logstash receive
* 5601: Kibana

## Execução

Após o clone do repositório, entre na pasta `monitoramento-elk` e execute:

```console
$ docker-compose up --build -d
```
ou
```console
$ ./start.sh
```

## Parar a execução

Caso deseje parar a execução da pilha, execute:

```console
$ docker-compose down
```
ou
```console
$ ./stop.sh
```

A opção **restart: always** está ativada ou seja, ao reiniciar o servidor, o ELK será iniciado sem nenhum tipo de intervenção.


## Acompanhar os logs em tempo real

```console
$ docker-compose logs -f 
```
ou
```console
$ ./logs.sh
```

## Bibliografia
1. [Elastic.co](https://www.elastic.co/pt/what-is/elk-stack)
2. [Readthedocs](https://elk-docker.readthedocs.io/)
3. [Medium](https://medium.com/@harisshafiq08/elk-stack-deployment-through-docker-compose-98ce40ff2fb6)
4. [Digital Ocean](https://www.digitalocean.com/community/tutorials/como-instalar-elasticsearch-logstash-e-kibana-elastic-stack-no-ubuntu-18-04-pt)
5. [Abraseucodigo](http://blog.abraseucodigo.com.br/melhorando-seus-logs-com-elk.html)
6. [Codechain](https://www.codechain.com.br/2019/01/07/elasticsearch-para-todos-parte-1/)
7. [Eduardo Neves]()

## Leituras extras e ferramentas

1. [Pipeline modular](https://www.elastic.co/pt/blog/how-to-create-maintainable-and-reusable-logstash-pipelines)
2. [Enviar dados para vários destinos](https://www.elastic.co/pt/blog/using-logstash-to-split-data-and-send-it-to-multiple-outputs)
3. [Debug para Grok](http://grokdebug.herokuapp.com/)
4. [Elastic Integrations](https://www.elastic.co/pt/integrations?solution=observability)

## Erros
### 1. "... exit 78"

Caso ocorra este erro ao subir a pilha, normalmente no momento do `elasticsearch`, execute o comando abaixo

```console
# sysctl -w vm.max_map_count=262144
```

Para tornar esta execução definitiva

```console
# echo 'vm.max_map_count=262144' >> /etc/sysctl.conf && sysctl -p
```

### 2. "Connection refused"

Este erro ocorre quando o `logstash` tenta acessar o `elasticsearch`, quando ambos foram criado pelo docker.

Informe no arquivo `.\files\logstash.conf`, no campo hosts do elasisticearch, o nome do container que está rodando o elasticsearch.

```console
output {
  stdout { codec => rubydebug }
  elasticsearch {
    hosts => "elasticsearch"
    index => "%{[http_poller_metadata][name]}"
  }
```
